import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class desafio {
    private WebDriver driver;

    @Before
    public void abrir(){
        //criando o acesso a página
        System.setProperty("webdriver.gecko.driver","C:/Drive/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
    }
    @After
    //método de sair
    public void sair() {
        driver.quit();
    }
    @Test
    public void desafio() throws  InterruptedException{
        //criando uma verificação para cada elemento da pagina.
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[1]/td/input")).sendKeys("Julhinho");
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[2]/td/input")).sendKeys("julhinhoQ.A123");
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[3]/td/font/textarea")).sendKeys("Fabrica de Software");
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[5]/td/input[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[6]/td/input[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[7]/td/select/option[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[9]/td/input[2]")).click();
        Assert.assertEquals("Processed Form Details", driver.findElement(By.xpath("/html/body/div/h1")).getText());
        Thread.sleep(5000);

        //Minha automação teve somente um problema com o "TextArea Comment:",não consegui solucionar o erro sozinho, fora isso rodou perfeitamente, se tirar ele."
    }
}
